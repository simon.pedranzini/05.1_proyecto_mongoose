const express = require('express');
const routes = require('./routes/index.routes'); // Se importa para hacer la ruta al INDEX
const { connectDb } = require('./config/db'); // Se importa para hacer la conexión a la BBDD
const movieRouter = require('./routes/movie.routes');
const cinemaRouter = require('./routes/cinema.routers');

const createMovieRouter = require('./routes/create-movie.routes');
const deteleMovieRouter = require('./routes/delete-movie.routes');
const modifyMovieRouter = require('./routes/modify-movie.routes');
const createCinemaRouter = require('./routes/create-cinema.routes');
const addMovieToCinema = require('./routes/add-movie-cinema.routes');

connectDb(); // Conexión a la BBDD

const PORT = 3000;
const server = express();

// Esta línea, le dice a express que si recibe un POST, PUT con JSON adjunto,
// lo lea y lo mande al endpoint dentro del req.body
server.use(express.json());
// si viene en POST,PUT y viene ne formato form-urlencodde, meta la informacion en req.body
server.use(express.urlencoded({ extended : false }));



server.use('/', routes); // pagina inicial
server.use('/peliculas', movieRouter); // show peliculas
server.use('/pelicula', createMovieRouter);
server.use('/pelicula', deteleMovieRouter);
server.use('/pelicula', modifyMovieRouter);
server.use('/cines', cinemaRouter); // Show cines
server.use('/cines', createCinemaRouter);
server.use('/cine', addMovieToCinema)


// Controlador de ERrores (Error de habdler) Siempre debajo de las rutas
server.use((error, req, res, next ) => { // NEXT PARTE MAS IMPORTANTE DE EXPRESS
    // error.mesage = 500
    //error.status = 'Unexpected Error';
    // al usar next(), la ejecución llega a este punto.
    // si no se usa next(), no se llega hasta aquí.
    const status = error.status || 500;
    const message = error.message || 'Unexpected error!';

    return res.status(status).json(message);
});


/*
CRUD (acsróstico)
Create
    Read
        Update
            Delete
*/

const callbackServidor = () =>  {
    console.log(`El servidor está arrancado en la dirección http://localhost:${PORT}`);
}

server.listen(PORT, callbackServidor);