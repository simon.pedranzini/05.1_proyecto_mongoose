const express = require('express');
const Cinemas = require('../models/Cinema');

const router = express.Router();

router.get('/', async (req, res, next) => {
    try {
        const cines = await Cinemas.find().populate('movies');
        return res.send(cines);
    } catch (error) {
        return next(error);
    }
})

module.exports = router;

