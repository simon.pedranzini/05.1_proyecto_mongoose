const express = require('express');
const Movies = require('../models/Movie')

const router = express.Router();

router.post('/creacion', async (req, res, next) => {
    try {
        const newMovie = new Movies({
                id: req.body.id,
                overview: req.body.overview,
                puntuacion: req.body.puntuacion,
                release_date: req.body.release_date,
                title: req.body.title,
        });

        const createdMovie = await newMovie.save()
        console.log(`La película "${createdMovie.title}" se ha añadido correctamente!!`);
        // SOLID PRINCIPLES -> S = Single Responsability Principale
        return res.status(200).json(createdMovie)

    } catch (error) {
        return res.next(error);
    }
});

module.exports = router;