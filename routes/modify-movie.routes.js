const express = require('express');
const Movie = require('../models/Movie');

const router = express.Router();

router.put('/editar/:id', async (req, res, next) => { // EN POSTMAN, BODY > RAW > JSON!
    try {
        const { id } = req.params;
        const movieModify = new Movie(req.body); // se recoje la pelicula con la info del body
        movieModify._id = id // al personaje creado se le mete la propiedad _id
        const movieUpdated = await Movie.findByIdAndUpdate(id, movieModify)
        return res.status(200).json(movieUpdated); //
    } catch {
        return next(error);
    }
})

module.exports = router;


// router.put('/edit/:id', async (req, res, next) => {
//     try {
//         const { id } = req.params //Recuperamos el id de la url
//         const characterModify = new Character(req.body) //instanciamos un nuevo Character con la información del body
//         characterModify._id = id //añadimos la propiedad _id al personaje creado
//         const characterUpdated = await Character.findByIdAndUpdate(id , characterModify)
//         return res.status(200).json(characterUpdated)//Este personaje que devolvemos es el anterior a su modificación
//     } catch (error) {
//         return next(error)
//     }
// })