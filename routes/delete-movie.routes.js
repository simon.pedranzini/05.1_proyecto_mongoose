const express = require('express')
const Movies = require('../models/Movie')

const router = express.Router();

router.delete('/eliminar/:id', async (req, res, next) => {
    try {
    const { id } = req.params;
    await Movies.findByIdAndDelete(id);
    return res.status(200).json(`La pelicula ha sido borrada correctamente!`);
    }
    catch (error) {
        return next(error);
    }
});


module.exports = router;




// router.delete('/:id', async (req, res, next) => {
//     try {
//         const {id} = req.params;
//         // No será necesaria asignar el resultado a una variable ya que vamos a eliminarlo
//         await Character.findByIdAndDelete(id);
//         return res.status(200).json('Character deleted!');
//     } catch (error) {
//         return next(error);
//     }
// });