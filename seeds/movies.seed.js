const mongoose = require('mongoose'); // Para hacer la conexión
const Movie = require('../models/Movie') // Para importar el "molde", modelo de cada película
const { DB_URL, CONFIG_DB } = require("../config/db"); // Para hacer la conexión con los datos de la BBDD
const Movies = require('../models/Movie');

// Hay que ejecutar este fichero para cargar los datos a la DB.

const moviesArray = [
        {
            "id": 566525,
            "overview": "Adaptación cinematográfica del héroe creado por Steve Englehart y Jim Starlin en 1973, un personaje mitad chino, mitad americano, cuyo característico estilo de combate mezclaba kung-fu, nunchacos y armas de fuego.",
            "puntuacion": 4,
            "release_date": 2021,
            "title": "Shang-Chi y la leyenda de los Diez Anillos",
        },
        {
            "id": 580489,
            "overview": "Eddie Brock (Tom Hardy) y su simbionte Venom todavía están intentando descubrir cómo vivir juntos cuando un preso que está en el corredor de la muerte (Woody Harrelson) se infecta con un simbionte propio.",
            "puntuacion": 2,
            "release_date": 2020,
            "title": "Venom: Habrá Matanza",
        },
        {
            "id": 370172,
            "overview": "En ‘No Time to Die’, James Bond se encuentra disfrutando de unas merecidas vacaciones en Jamaica. Sin embargo, su paz termina cuando su amigo de la CIA, Felix Leiter, lo busca para una nueva misión que implica rescatar a un importante científico que ha sido secuestrado.",
            "puntuacion": 3,
            "release_date": 2020,
            "title": "Sin tiempo para morir",
        },
        {
            "id": 585245,
            "overview": "Cuando Emily Elizabeth conoce a un rescatador mágico de animales que le regala un pequeño cachorro rojo, nunca se hubiera imaginado que al despertarse se encontraría un sabueso gigante de tres metros en su pequeño apartamento de Nueva York. Mientras su madre soltera se encuentra de viaje de negocios, Emily y su divertido pero impulsivo tío Casey se embarcan en una gran aventura.",
            "puntuacion": 2,
            "release_date": 2019,
            "title": "Clifford, El Gran Perro Rojo",
        },
        {
            "id": 763164,
            "overview": "Cinco cazadores de élite pagan para cazar a un hombre en una isla desierta. A medida que avanza la cacería, los cazadores se convierten en presa de su presa que demuestra un nivel de supervivencia desconocido.",
            "puntuacion": 4,
            "release_date": 2018,
            "title": "Apex",
        },
        {
            "id": 634649,
            "overview": "Peter Parker está desenmascarado y ya no puede separar su vida normal de las altas apuestas de ser un superhéroe. Cuando le pide ayuda al Doctor Strange, lo que está en juego se vuelve aún más peligroso, lo que lo obliga a descubrir lo que realmente significa ser Spider-Man.",
            "puntuacion": 3,
            "release_date": 2021,
            "title": "Spider-Man: No Way Home",
        },
        {
            "id": 438631,
            "overview": "En un lejano futuro, la galaxia conocida es gobernada mediante un sistema feudal de casas nobles bajo el mandato del Emperador. Las alianzas y la política giran entorno a un pequeño planeta, Dune,  del que extrae la \"especia melange\", la materia prima que permite los viajes espaciales. La Casa Atreides, bajo el mandato del Duque Leto Atreides recibe el encargo de custodiar el planeta, relevando en la encomienda a sus históricos enemigos, los Harkonnen. Paul Atreides, hijo del duque, se verá atrapado en las intrigas políticas mientras descubre el destino que le deparan los desiertos de Dune. Nueva adaptación al cine de las novelas de Frank Herbert, que ya fueron trasladadas a la gran pantalla por David Lynch en 1984.",
            "puntuacion": 3,
            "release_date": 2021,
            "title": "Dune",
        },
        {
            "id": 550988,
            "overview": "Guy trabaja como cajero de un banco, y es un tipo alegre y solitario al que nada la amarga el día. Incluso si le utilizan como rehén durante un atraco a su banco, él sigue sonriendo como si nada. Pero un día se da cuenta de que Free City no es exactamente la ciudad que él creía. Guy va a descubrir que en realidad es un personaje no jugable dentro de un brutal videojuego.",
            "puntuacion": 1,
            "release_date": 2021,
            "title": "Free Guy",
        },
        {
            "id": 568124,
            "overview": "Encanto, te traslada a Colombia, donde una mágica familia vive en una casa llena de magia.",
            "puntuacion": 5,
            "release_date": 2021,
            "title": "Encanto",
        },
        {
            "id": 574060,
            "overview": "Eva (Karen Gillan) era lo más importante en la turbulenta vida de su madre, Scarlet (Lena Headey). Pero sus enemigos obligaron a Scarlet a huir abandonándolo todo, incluida a su hija. Años más tarde, Eva se convierte en una asesina a sangre fría siguiendo los pasos de su madre. Después de perder el control en una misión, poniendo a una inocente niña de 8 años en peligro, Eva no tiene más remedio que enfrentarse sin escrúpulos a sus antiguos compinches. Y Scarlet y su banda no tienen más remedio que volver para intentar ayudarla.",
            "puntuacion": 4,
            "release_date": 2021,
            "title": "Gunpowder Milkshake (Cóctel explosivo)",
        },
        {
            "id": 635302,
            "overview": "Tanjiro y sus compañeros se unen al Pilar de las Llamas Kyojuro Rengoku para investigar una misteriosa serie de desapariciones que han ocurrido dentro del “Tren Infinito”. Poco saben que Enmu, uno de los miembros de las Doce Lunas Demoníacas, también está a bordo y les ha preparado una trampa.",
            "puntuacion": 4,
            "release_date":  2020,
            "title": "Guardianes de la Noche: Tren infinito",
        },
        {
            "id": 451048,
            "overview": "Principios del siglo XX. Frank es el carismático capitán de una peculiar embarcación que recorre la selva amazónica. Allí, a pesar de los peligros que el río Amazonas les tiene preparados, Frank llevará en su barco a la científica Lily Houghton y a su hermano McGregor Houghton. Su misión será encontrar un árbol místico que podría tener poderes curativos. Claro que su objetivo no será fácil, y en su aventura se encontrarán con toda clase de dificultades, además de una expedición alemana que busca también este árbol con propiedades curativas. Esta comedia de acción y aventuras está basada en la atracción Jungle Cruise de los parques de ocio de Disney.",
            "puntuacion": 5,
            "release_date": 2022,
            "title": "Jungle Cruise",
        },
        {
            "id": 425909,
            "overview": "Una madre soltera y sus dos hijos se van a vivir a un pequeño pueblo donde descubrirán su conexión con los orígenes de los Cazafantasmas y el legado secreto que su abuelo les ha dejado.",
            "puntuacion": 1,
            "release_date": 2019,
            "title": "Cazafantasmas: Más allá",
        },
        {
            "id": 714968,
            "overview": "Alemania 1945, Max, un sobreviviente judío del Holocausto, se encuentra con un grupo radical de combatientes de la resistencia judíos que, como él, perdieron toda esperanza en su futuro después de que les robaron su existencia y sus familias enteras fueron asesinadas por los nazis. Sueñan con represalias a una escala épica para el pueblo judío. Ojo por ojo, diente por diente. Max comienza a identificarse con los monstruosos planes del grupo ...",
            "puntuacion": 2,
            "release_date": 2016,
            "title": "Plan A",
        },
        {
            "id": 675445,
            "overview": "La patrulla canina está en racha. Cuando Humdinger, su mayor rival, se convierte en alcalde de la cercana Ciudad Aventura y empieza a causar estragos, Ryder y los heroicos cachorros se ponen en marcha para enfrentarse a este nuevo desafío. Mientras uno de los cachorros debe enfrentarse a su pasado en Ciudad Aventura, el equipo encuentra ayuda en una nueva aliada, la inteligente perrita salchicha Liberty. Juntos y armados con nuevos y emocionantes artefactos y equipos, la patrulla canina lucha por salvar a los ciudadanos de Ciudad Aventura.",
            "puntuacion": 3,
            "release_date": 2017,
            "title": "La patrulla canina: La película",
        },
        {
            "id": 379686,
            "overview": "Secuela de la cinta original de 1996, Space Jam, protagonizada por Michael Jordan. En esta segunda parte, la super estrella de la NBA es LeBron James, quien queda atrapado junto a su hijo Dom en un extraño lugar, un espacio digital de una fuerza todopoderosa y malvada conocida como A.I. Para volver a casa y poner a salvo a su hijo, el jugador de baloncesto deberá unir fuerzas con Bugs Bunny, Lola Bunny y el resto de personajes de los Looney Tunes para enfrentarse en un partido de baloncesto a los campeones digitalizados por inteligencia artificial.",
            "puntuacion": 4,
            "release_date": 2018,
            "title": "Space Jam: Nuevas Leyendas",
        },
        {
            "id": 675319,
            "overview": "En un futuro cercano, en una colonia de robots de última generación, el creador de la colonia contrata a un investigador privado para llevar a casa a su hija desaparecida.",
            "puntuacion": 3,
            "release_date": 2019,
            "title": "Zona 414",
        },
        {
            "id": 576845,
            "overview": "Thriller psicológico sobre una joven apasionada por la moda que misteriosamente puede entrar en la década de 1960, donde se encuentra con su ídolo, un atractivo aspirante a cantante. Pero el Londres de los sesenta no es lo que parece, y el tiempo comenzará a desmoronarse con sombrías consecuencias.",
            "puntuacion": 4,
            "release_date": 2020,
            "title": "Última Noche en el Soho",
        },
        {
            "id": 529203,
            "overview": "Después de salir de su cueva, los Croods se encuentran con su mayor amenaza desde que se fueron: otra familia llamada Betterman, quienes afirman y demuestran ser mejores y evolucionados. Grug empieza a sospechar de los padres de Betterman, Phil y Hope, ya que planean en secreto separar a su hija Eep con su amado novio Guy para asegurarse de que su hija Dawn tenga un compañero amoroso e inteligente que la proteja.",
            "puntuacion": 2,
            "release_date": 2021,
            "title": "Los Croods: Una nueva era",
        }
    ];

    mongoose.connect(DB_URL, CONFIG_DB)
        .then(async () => {
            console.log(`Ejecutando movies.seed.js ........ `);
            const allMovies = await Movies.find();
                if(allMovies.length) {
                    await Movies.collection.drop();
                    console.log(`Colección de películas eliminada successfully!`);
                }
        }).catch( error => console.log(`Error buscando en la Base de Datos ${error}`))
            .then(async () => {
                console.log(`Añadiendo las películas de su seed.js`);
                await Movies.insertMany(moviesArray);
                console.log(`Peliculas añadidas Successfully!`);
            })
            .catch(error => console.log(`Error añadiendo las nuevas películas ${error}`))
            .finally(() => mongoose.disconnect(console.log(`Cerrando conexión. Bye`)));
