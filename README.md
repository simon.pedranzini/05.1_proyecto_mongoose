Proyecto 1 NODE

Primer Commit => index.js and git

Segundo Commit => nodemon OK

Tercer Commit => Rutas '/', Index.html OK

Cuarto Commit => Modelo de peliculas, Introducción de datos en la DB, GET a peliculas OK.

Quinto Commit => README.md updated.

Sexto Commit => Busqueda GET por ID y por titulo

Septimo Commit => Introduccion de datos por POST (body)

Octavo Commit -> Model Cinemas, Listado y añadir nuevos OK

Noveno Commit -> PUT/PUSH, relacionar dos modelos y visualizarlos mediante populate.

NECESIDADES:

npm install

git init

npm install express

npm install mongoose



INFORMACIÓN:

Para escribir en la base de datos => ejecutar fichero movies.seed.js

movies.routes.js para leer el contenido actual => localhost:3000/peliculas


Buscar película por ID => localhost:3000/peliculas/<id>

Buscar película por titulo => localhost:3000/peliculas/titulo/<titulo>